# 👋 Hello World

## 📚 Description
A basic web server serving a static `Hello World` message packaged up into a Docker container.

Can be used as a demo or placeholder image 🧪.

## 🏃‍♂️ Running

Clone [the repo from GitLab][1] and build with `docker build -t tobiasgray/hello-world .` then run with `docker run -p 8080:8080 hello`. Or pull directly from [DockerHub][2] via `docker pull tobiasgray/hello-world`.

Then run via `docker run -p 8080:8080 tobiasgray/hello-world`.

You should then be able to access the "Hello World" page at http://localhost:8080.

### 🔧 Extra Configuration

Change the port the web server runs on by passing the `PORT` environment variable.

```
docker run -e PORT 3000 -p 3000:3000 tobiasgray/hello-world
```


[1]: https://gitlab.com/tobias.gray/docker-hello-world
[2]: https://hub.docker.com/r/tobiasgray/hello-world